#!/bin/bash

echo DB_VM starting provisioning
# '''
# Necessary stuff for automation of the mysql installation without interaction, 
# setting the mysql password and whatnot before the installation 
# '''

export DEBIAN_FRONTEND="noninteractive"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $DB_PASS"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $DB_PASS"

apt-get update >/dev/null
apt-get install mysql-server -y


mysql -u root -p$DB_PASS < sql/user.sql
mysql -u root -p$DB_PASS petclinic < sql/schema.sql
mysql -u root -p$DB_PASS petclinic < sql/data.sql

sed -i 's/bind/#bind/' /etc/mysql/mysql.conf.d/mysqld.cnf

sudo systemctl restart mysql.service