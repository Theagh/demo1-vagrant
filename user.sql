CREATE USER petclinic IDENTIFIED BY 'petclinic';

CREATE DATABASE IF NOT EXISTS petclinic;

ALTER DATABASE petclinic
  DEFAULT CHARACTER SET utf8
  DEFAULT COLLATE utf8_general_ci;

GRANT ALL PRIVILEGES ON petclinic.* TO 'petclinic'@'192.168.33.%' IDENTIFIED BY 'petclinic';

DROP USER petclinic;