#!/bin/bash

apt-get update >/dev/null
apt-get install default-jdk git  -y
apt-get install mysql-client-core-5.7 -y

#check if already cloned or not
if [[ ! -d "/home/vagrant/demo1" ]]
then
    git clone https://gitlab.com/Theagh/demo1.git
fi

while [[ ! -d "demo1/src/main/resources" ]];do
    echo '...\r'
done
sed -i "s/localhost/$DB_HOST/"  demo1/src/main/resources/application-mysql.properties


chmod +x demo1/mvnw
cd demo1

./mvnw -ntp -B package 

if [[ -d target ]];then
    java -Dspring.profiles.active=mysql -jar target/*.jar &
    echo "Spring app running on $APP_HOST:8080"
else
    echo "Maven build failed"
fi